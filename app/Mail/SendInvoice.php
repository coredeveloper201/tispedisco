<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendInvoice extends Mailable
{
    use Queueable, SerializesModels;
    private $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $order_code = $this->order->order_code;
        return $this->view('Admin.invoice.invoiceMail')
            ->from(['address' => 'invoice@tispedisco.com', 'name' => 'TiSpedisco Invoice'])
            ->subject("Invoice for #$order_code")
            ->with([
                'order' => $this->order,
            ]);
    }
}
