<?php

namespace App\Http\Controllers\BackEndCon;

use App\DataTables\OrdersDataTable;
use App\Http\Controllers\Controller;
use App\Mail\SendInvoice;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(OrdersDataTable $order, Request $request)
    {
        return $order->setData($request->all())->render('Admin.orders.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::with('items')->find($id);
        return view('Admin.orders.single', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id)->delete();
        if ($order) {
            return response()->json(['success' => true, 'message' => "Successful"], 200);
        } else {
            return response()->json(['success' => false, 'message' => "Something Went wrong!"], 200);
        }
    }

    public function changeStatus(Request $request)
    {
        $order = Order::findOrFail($request->id);
        $update = $order->update(['status' => $request->status]);
        if ($update) {
            $this->sendInvoiceMail($order);
            return response()->json(['success' => true, 'message' => "Order Status Changed"], 200);
        } else {
            return response()->json(['success' => false, 'message' => "Something Went wrong!"], 200);
        }
    }

    public function sendInvoiceMail($order){
        Mail::to($order->sender_email)->send(new SendInvoice($order));
        return response()->json(['success' => true, 'message' => "Email Send"], 200);
    }
}
