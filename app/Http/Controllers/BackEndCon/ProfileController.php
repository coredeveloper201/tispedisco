<?php

namespace App\Http\Controllers\BackEndCon;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function changePasswordView()
    {
        return view('Admin.profile.change-password');
    }

    public function changePassword(Request $request)
    {
        $user = auth()->user();
        $validatedData = $request->validate([
            'current_password' => ['required', 'string', 'min:8'],
            'password'         => ['required', 'string', 'min:8', 'confirmed']
        ]);
        if (!Hash::check($validatedData['current_password'], $user->password)) {
            return back()->with('error', 'La password non è corretta.');
        }
        $user->update([
            'password' => Hash::make($request->password),
        ]);
        return back()->with('success', 'La tua password è stata modificata.');
    }
}
