<?php

namespace App\Http\Controllers\BackEndCon;

use App\Http\Controllers\Controller;
use App\Order;
use App\Ticket;
use App\User;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $total_users   = User::count();
        $total_orders  = Order::count();
        $total_tickets = Ticket::count();
        $data = array(
            'total_users'   => $total_users,
            'total_orders'  => $total_orders,
            'total_tickets' => $total_tickets,
        );
        return view('Admin.index', compact('data'));
    }
}
