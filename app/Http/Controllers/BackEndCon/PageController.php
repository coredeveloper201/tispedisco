<?php

namespace App\Http\Controllers\BackEndCon;

use App\DataTables\PagesDataTable;
use App\Http\Controllers\Controller;
use App\CustomPage;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PagesDataTable $page, Request $request)
    {
        return $page->setData($request->all())->render('Admin.pages.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = new CustomPage();
        return view('Admin.pages.from', compact('page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'page_title'       => ['required'],
            'slug'             => ['required'],
            'meta_title'       => ['required'],
            'meta_description' => ['required'],
            'content'          => ['required'],
        ]);
        $page = CustomPage::create($validatedData);
        if ($page){
            session()->flash('success', 'Custom Page Created Successfully');
        }else {
            session()->flash('error', 'Whoops! Something went wrong');
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = CustomPage::findOrFail($id);
        return view('Admin.pages.from', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'page_title'       => ['required'],
            'slug'             => ['required'],
            'meta_title'       => ['required'],
            'meta_description' => ['required'],
            'content'          => ['required'],
        ]);
        $page = CustomPage::findOrFail($id);
        $update = $page->update($validatedData);
        if ($update){
            session()->flash('success', 'Custom Page Updated Successfully');
        }else {
            session()->flash('error', 'Whoops! Something went wrong');
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = CustomPage::find($id)->delete();
        if ($page) {
            return response()->json(['success' => true, 'message' => "Page Deleted Successfully"], 200);
        } else {
            return response()->json(['success' => false, 'message' => "Something Went wrong!"], 200);
        }
    }
}
