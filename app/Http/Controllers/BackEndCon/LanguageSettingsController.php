<?php

namespace App\Http\Controllers\BackEndCon;

use File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LanguageSettingsController extends Controller
{
    public function createLanguage(Request $request)
    {

        if (!array_key_exists($request->locale, config('languages'))) {
            config(['languages.'. $request->locale => $request->name]);
            $fp = fopen(base_path() .'/config/languages.php' , 'w');
            fwrite($fp, '<?php return ' . var_export(config('languages'), true) . ';');
            fclose($fp);
            File::makeDirectory(resource_path('lang') . '/' . $request->locale, $mode = 0777, true, true);

            return redirect()
                ->route('languages.index')
                    ->with('success', __('translation::translation.language_added'));
        }else{
            return redirect()
                ->route('languages.index')
                    ->with('error', 'Language already exists.');
        }
    }
}
