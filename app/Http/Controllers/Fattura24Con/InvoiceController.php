<?php

namespace App\Http\Controllers\Fattura24Con;

use App\Http\Controllers\Controller;
use App\Invoice;
use App\Order;
use App\orderItem;
use App\Rate;
use Carbon\Carbon;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
    }

    public function createInvoice($order)
    {
        $paymentDetails = session('paymentDetails');
        $paymentMethod = $this->getPaymentMethod();
//        $shippingAddress = Order::find(7);
//        $order_items = OrderItem::where('order_id', 7)->get();
        $shippingAddress     = Order::find($order['id']);
        $order_items         = OrderItem::where('order_id', $order['id'])->get();
        $vat                 = $this->getVatAmount($shippingAddress["total_cost"]);
        $xml_order_item_rows = "";
        foreach ($order_items as $key => $order_item) {
            $volume = $order_item->height * $order_item->width * $order_item->length;
            $xml_order_item_rows .= "<Row>";
            $xml_order_item_rows .= "<Code>" . $order_item->id . "</Code>";
            $xml_order_item_rows .= "<Description>" . $order_item->description . "</Description>";
            $xml_order_item_rows .= "<Qty>1</Qty>";
            $xml_order_item_rows .= "<Um></Um>";
            $xml_order_item_rows .= "<Price>" . $order_item->price . "</Price>";
            $xml_order_item_rows .= "<Discounts></Discounts>";
            $xml_order_item_rows .= "<VatCode>" . $shippingAddress['sender_vat_no'] . "</VatCode>";
//            $xml_order_item_rows .= "<VatDescription>15%</VatDescription>";
            $xml_order_item_rows .= "<VatDescription>" . $this->getRateFromVolume($volume)->vat . "%</VatDescription>";
            $xml_order_item_rows .= "</Row>";
        }

        if (isset($shippingAddress["isInvoice"]) && $shippingAddress["isInvoice"] == "on") {
            $isInvoice = "true";
        } else {
            $isInvoice = "false";
        }

        $xmlstring = '<Fattura24>';
        $xmlstring .= '<Document>';
        $xmlstring .= '<DocumentType>I</DocumentType>';
        $xmlstring .= '<CustomerName>' . $shippingAddress["sender_full_name"] . '</CustomerName>';
        $xmlstring .= '<CustomerAddress>' . $shippingAddress["sender_address_1"] . '</CustomerAddress>';
        $xmlstring .= '<CustomerPostcode>' . $shippingAddress["sender_postcode"] . '</CustomerPostcode>';
        $xmlstring .= '<CustomerCity>' . $shippingAddress["sender_city"] . '</CustomerCity>';
        $xmlstring .= '<CustomerProvince>' . $shippingAddress["sender_province"] . '</CustomerProvince>';
        $xmlstring .= '<CustomerCountry>' . $shippingAddress["sender_country"] . '</CustomerCountry>';
        $xmlstring .= '<CustomerFiscalCode>MARROS66C44G217W</CustomerFiscalCode>';
        $xmlstring .= '<CustomerVatCode>' . $shippingAddress["sender_vat_no"] . '</CustomerVatCode>';
        $xmlstring .= '<CustomerCellPhone>' . $shippingAddress["sender_phone"] . '</CustomerCellPhone>';
        $xmlstring .= '<CustomerEmail>' . $shippingAddress["sender_email"] . '</CustomerEmail>';
        $xmlstring .= '<DeliveryName>' . $shippingAddress["receiver_full_name"] . '</DeliveryName>';
        $xmlstring .= '<DeliveryAddress>' . $shippingAddress["receiver_address_1"] . '</DeliveryAddress>';
        $xmlstring .= '<DeliveryPostcode>' . $shippingAddress["receiver_postcode"] . '</DeliveryPostcode>';
        $xmlstring .= '<DeliveryCity>' . $shippingAddress["receiver_city"] . '</DeliveryCity>';
        $xmlstring .= '<DeliveryProvince>' . $shippingAddress["receiver_province"] . '</DeliveryProvince>';
        $xmlstring .= '<DeliveryCountry>' . $shippingAddress["receiver_country"] . '</DeliveryCountry>';
        $xmlstring .= '<Object></Object>';
        $xmlstring .= '<TotalWithoutTax>' . $shippingAddress["total_cost"] . '</TotalWithoutTax>';
        $xmlstring .= '<PaymentMethodName>' . $paymentMethod->method_name . '</PaymentMethodName>';
        $xmlstring .= '<PaymentMethodDescription>' . $paymentMethod->description . '</PaymentMethodDescription>';
        $xmlstring .= '<VatAmount>' . $vat . '</VatAmount>';
        $xmlstring .= '<Total>' . ((double)$shippingAddress["total_cost"] + $vat) . '</Total>';
        $xmlstring .= '<FootNotes>' . $paymentDetails["possible_notes"] . '</FootNotes>';
        $xmlstring .= '<SendEmail>' . $isInvoice . '</SendEmail>';
        $xmlstring .= '<UpdateStorage>1</UpdateStorage>';
        $xmlstring .= '<F24InvoiceId>12345</F24InvoiceId>';
        $xmlstring .= '<IdTemplate>123</IdTemplate>';
        $xmlstring .= '<CustomField1></CustomField1>';
        $xmlstring .= '<CustomField2></CustomField2>';
        $xmlstring .= '<Payments>';
        $xmlstring .= ' <Payment>';
        $xmlstring .= '    <Date> ' . Carbon::now()->format("Y-m-d") . '</Date>';
        $xmlstring .= '    <Amount> ' . ((double)$shippingAddress["total_cost"] + $vat) . '</Amount>';
        $xmlstring .= '    <Paid> true</Paid>';
        $xmlstring .= '  </Payment>';
        $xmlstring .= '</Payments>';
        $xmlstring .= '<Rows>' . $xml_order_item_rows . '</Rows>';
        $xmlstring .= '</Document>';
        $xmlstring .= '</Fattura24>';

//        dd($xmlstring);

        $xw = xmlwriter_open_memory();
        xmlwriter_start_document($xw, '1.0', 'UTF-8');
        xmlwriter_text($xw, $xmlstring);

        $xml = xmlwriter_output_memory($xw);

        // Make the http call to fattura24 api
        $xml_res = $this->createDocument('creates', $xml);

        /* LEGGO I DATI RICEVUTI DA FATTURA24 */
        $xml = simplexml_load_string($xml_res);

        if ($xml->returnCode == 0) {
            $invoice              = new Invoice();
            $invoice->returnCode  = $xml->returnCode;
            $invoice->description = $xml->description;
            $invoice->docId       = $xml->docId;
            $invoice->docNumber   = $xml->docNumber;
            $invoice->order_id    = $shippingAddress->id;
            $invoice->save();
        } else {
            return ['success' => false, 'message' => 'Whoops! Something Went Wrong with Fattura24'];
        }
        return ['success' => true, 'message' => 'Fattura API Successful'];
    }

    /**
     * Make the http call to fattura24 api.
     *
     * @return \Illuminate\Http\Response
     */
    private function createDocument($action, $xml)
    {
        $efatt_api_key = env('FATTURA24_API_KEY');

        switch ($action) {
            // Create map with request parameters
            case 'test':
                $action = '/TestKey';
                $params = array('apiKey' => $efatt_api_key);
                break;
            case 'creates':
                $action = '/SaveDocument';
                $params = array('apiKey' => $efatt_api_key, 'xml' => $xml);
                break;
        }


        $api_url = 'https://www.app.fattura24.com/api/v0.3';

        // Build Http query using params
        $query = http_build_query($params);

        // Create Http context details
        $contextData = array(
            'method' => 'POST',
            'header' => "Content-Type: application/x-www-form-urlencoded\r\n" .
                "Connection: close\r\n" .
                "Content-Length: " . strlen($query) . "\r\n",
            'content' => $query);

        // Create context resource for our request
        $context = stream_context_create(array('http' => $contextData));

        // Read page rendered as result of your POST request
        $result = file_get_contents(
            $api_url . $action,  // page url
            false,
            $context);


        // Server response is now stored in $result variable so you can process it
        $result = html_entity_decode($result);
        return $result;
    }

    private function getVatAmount($total_cost = 0)
    {
        $locations = session('locations');
        if (isset($locations)) {
            $rate = Rate::where('distance_from', '<=', $locations['distance'])
                ->where('distance_to', '>=', $locations['distance'])
                ->where('weight_to', '>=', $locations['weight'])->first();
            if ($rate) {
                if ($rate->vat != 0) {
                    $total_cost += $total_cost * $rate->vat / 100;
                }
            }
        } else {
            $total_cost = 0;
        }

        return $total_cost;
    }

    private function getPaymentMethod()
    {
        $payment_method = array(
            'method_name' => "Stripe",
            "description" => "IBAN: IT02L1234512345123456789012",
        );
        $payment_method = collect([(object)$payment_method]);
        return $payment_method[0];
    }

    private function getRateFromVolume($volume = 0)
    {
        $locations = session('locations');
        if (isset($locations)) {
            return Rate::where('distance_from', '<=', $locations['distance'])
                ->where('distance_to', '>=', $locations['distance'])
                ->where('volume', '>=', $volume)->first();
        } else {
            return null;
        }
    }
}
