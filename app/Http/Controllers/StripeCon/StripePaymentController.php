<?php

namespace App\Http\Controllers\StripeCon;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\User;

class StripePaymentController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'verified']);

        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET_KEY'));

        if (env('APP_ENV') == 'production' )
            \Stripe\Stripe::setApiKey(env('STRIPE_PUBLISHABLE_KEY'));

        if (env('APP_ENV') == 'local' )
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET_KEY'));
    }

    public function createPayment($token, $chargeData, $user = null)
    {
        if (!$token) {
            return ['success' => false, 'message' => 'Invalid token.'];
        }

        $validator = Validator::make($chargeData, [
            "amount" => 'required|numeric|min:0',
        ]);

        if ($validator->fails()) {
            return ['success' => false, 'message' => $validator->errors()];
        }

        $validatedData = $validator->validate();

        // create new payment;
        $data = $validatedData;
        $data['currency'] = $validatedData['currency'] ?? 'usd';
        $data['source'] = $token;

        try {
            if ($user) {
                $responseData = app('\App\Http\Controllers\StripeCon\StripeCustomerController')->getCustomerId($user);  
                if ($responseData['success'] == false) {
                    return $responseData;
                }        
                $customerId = $responseData['customerId'];

                $cardData = session()->get('paymentDetails');
                $responseData = app('\App\Http\Controllers\StripeCon\StripeCardController')->attachCard($user, $cardData);
                if ($responseData['success'] == false) {
                    return $responseData;
                }
                             
                $data['source'] = $responseData['card']['id'];                
                $data['customer'] = $customerId;
            }

            $charge = \Stripe\Charge::create($data);

            return ['success' => true, 'message' => 'Customer payment successful.', 'charge' => $charge];
        } catch (\Exception $e) {
            return ['success' => false, 'message' => $e->getMessage()];
        }
    }
}
