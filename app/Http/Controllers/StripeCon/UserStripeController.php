<?php

namespace App\Http\Controllers\StripeCon;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserStripeController extends Controller
{
    public function getCards(Request $request)
    {
        $user = auth()->user();
        $responseData = app('\App\Http\Controllers\StripeCon\StripeCardController')->getCards($user);
        return response()->json($responseData);
    }

    public function getCustomer(Request $request)
    {
        $user = auth()->user();
        $responseData = app('\App\Http\Controllers\StripeCon\StripeCustomerController')->getCustomer($user);
        return response()->json($responseData);
    }

    public function getSingleCard(Request $request)
    {
        $user = auth()->user();
        $responseData = app('\App\Http\Controllers\StripeCon\StripeCardController')->getSingleCard($request->cardId, $user);
        return response()->json($responseData);
    }

    public function updateCard(Request $request)
    {
        $user = auth()->user();
        $updateData = array(
            'exp_month' => explode("/", $request->card_exp)[0],
            'exp_year' => explode("/", $request->card_exp)[1],
        );
        $responseData = app('\App\Http\Controllers\StripeCon\StripeCardController')->updateCard($request->cardId, $updateData, $user);
        return response()->json($responseData);
    }
}
