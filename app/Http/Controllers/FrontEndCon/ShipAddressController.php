<?php

namespace App\Http\Controllers\FrontEndCon;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class ShipAddressController extends Controller
{

    public function index()
    {
        if (!session('locations')) {
            return redirect()->route('root')->with('error', 'No location data found.');
        }

        return view('User.ship-address');
    }

    public function store(Request $request)
    {
        if (isset($request->sender->vat_no)) {
            $this->validate($request, [
                'vat_no' => 'numeric',
            ]);
        }


        $shippingAddress = $request->all();
        unset($shippingAddress['_token']);

        session(['shippingAddress' => $shippingAddress]);

        return redirect()->route('payment-design.index');
    }
}
