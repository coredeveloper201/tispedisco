<?php

namespace App\Http\Controllers\FrontEndCon;

use App\Http\Controllers\Controller;
use App\Invoice;
use App\Order;
use App\orderItem;
use App\Rate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rules\In;
use Illuminate\Validation\ValidationException;

class InvoiceController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth:admin');
    }

    public function index()
    {
        // Get all resources
        $invoices = Invoice::all()->sortByDesc('id');

        return view('admin.invoice.invoices', compact('invoices'));
    }

    public function store(Request $request)
    {
        $locations = session('locations');
        if (!isset($locations)) {
            abort(404);
        }

        // Stripe functionality are defined in PaymentDesignController
        $order_success = app('\App\Http\Controllers\FrontEndCon\PaymentDesignController')->storeSave($request->all());
        $order_success['success'] = true;
        $order = session('order');


        // if ($order_success['success']) {
        //     $fattura_invoice = app('\App\Http\Controllers\Fattura24Con\InvoiceController')->createInvoice($order);
        //     if (!$fattura_invoice['success']) {
        //         session()->flash('error', $fattura_invoice['message']);
        //     }
        // } else {
        //     session()->flash('error', $order_success['message']);
        // }

        return redirect(route('order-confirm.index'));
    }

    public function show(Invoice $invoice)
    {
        // Get the model
        $invoice = Invoice::find($invoice->id);

        return view('admin . invoice . invoice - show', compact('invoice'));
    }

    public function edit(Invoice $invoice)
    {
        //
    }

    public function update(Request $request, Invoice $invoice)
    {
        //
    }

    public function destroy(Invoice $invoice)
    {
        // Get the model
        Invoice::destroy($invoice->id);

        return redirect()->back()->with('success', 'Invoice deleted successfully . ');
    }
}
