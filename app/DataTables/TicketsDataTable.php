<?php

namespace App\DataTables;

use App\Ticket;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Html\Editor\Editor;

class TicketsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    protected $data;
    protected $user;

    public function __construct()
    {
        $this->user = Auth::guard('admin')->user();
    }

    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', 'Admin.tickets.action')
            ->addColumn('collection_date', function ($order) {
                if ($order->collection_date) {
                    return Carbon::parse($order->collection_date)->format('d-m-Y');
                } else {
                    return '';
                }
            })
            ->addColumn('status', function ($order) {
                if ($order->status == 'unread') {
                    return '<button type="button" class="btn btn-rounded btn-sm btn-block btn-info" style="max-width: 80px">Pending</button>';
                } elseif ($order->status == 'new') {
                    return '<button type="button" class="btn btn-rounded btn-sm btn-block btn-success" style="max-width: 80px">Active</button>';
                } else {
                    return '<button type="button" class="btn btn-rounded btn-sm btn-block btn-cyan" style="max-width: 80px">Send</button>';
                }
            })
            ->addColumn('state', function ($order) {
                if ($order->state == 'open') {
                    return '<button type="button" class="btn btn-rounded btn-sm btn-block btn-info" style="max-width: 80px">Open</button>';
                } elseif ($order->state == 'closed') {
                    return '<button type="button" class="btn btn-rounded btn-sm btn-block btn-success" style="max-width: 80px">Closed</button>';
                } else {
                    return '<button type="button" class="btn btn-rounded btn-sm btn-block btn-cyan" style="max-width: 80px">Undefined</button>';
                }
            })
            ->escapeColumns([]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Ticket $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Ticket $model)
    {
        $model = $model->newQuery();
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $search = "Search: "; // We can also use variables; This is for instruction purpose only
        $page_length = 10; // We can make it dynamic dependent on User
        $row_text = "";
        $need_input_columns = "[0,1]"; // We have to make the array as string to pass it because of array is is needed as string
        $builder = $this->builder();
        $suser_last_login = '';
        if (isset($this->data['suser_last_login'])) {
            $suser_last_login = $this->data['suser_last_login'];
        }
        $builder->postAjax([
            'url' => route('admin.tickets.index'),
            'data' => "function(d) { d.suser_last_login = '$suser_last_login'; }",
        ]);
        return $builder
            ->setTableId('user-table')
            ->columns($this->getColumns())
            ->dom('<"row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>rt<"row"<"col-sm-12 col-md-5"i><"col-sm-12 col-md-7"p>><"clear">')
            ->orderBy(1, 'asc')
            ->buttons(
                Button::make('create'),
                Button::make('export'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload')
            )
            ->parameters(array(
                'language' => array(
                    'lengthMenu' => '_MENU_ records',
                    'search' => $search,
                    'info' => 'Showing _START_ to _END_ of _TOTAL_ records',
                ),
                'lengthMenu' => array(
                    array(5, 10, 25, 50, 100, -1),
                    array('5' . $row_text, '10' . $row_text, '25' . $row_text, '50' . $row_text, '100' . $row_text, 'Show all')
                ),
                'pagingType' => "full_numbers",
                'pageLength' => $page_length,
                'createdRow' => "function (row, data, dataIndex ) {
                    $(row).attr('id', 'tr-' + data.id);
                }",
                'searchPlaceholder' => "Search...",
//                'initComplete' => "function () {
//                    this.api().columns($need_input_columns).every(function (colIdx) {
//                        var column = this;
//                        var title = $('tfoot').find('th').eq(colIdx).text();
//                        console.log($(column.footer()).empty());
//                        var input = document.createElement('input');
//                        // input.setAttribute('type', 'text');
//                        input.placeholder = title;
//                        $(input).appendTo($(column.footer()).empty())
//                        .on('change keyup clear', function () {
//                             column.search($(this).val(), false, false,true).draw();
//                        });
//                    });
//                }",
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('title', 'title')->title('Ticket Title')->footer('Ticket Title'),
            Column::make('status', 'status')->title('Status')->footer('Status'),
            Column::make('state', 'state')->title('State')->footer('State'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(180)
                ->addClass('text-center')
                ->footer('Actions'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'User_' . date('YmdHis');
    }

    public function setData($dataArray)
    {
        $this->data = $dataArray;
        return $this;
    }
}
