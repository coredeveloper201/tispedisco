<?php

namespace App\DataTables;

use App\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Html\Editor\Editor;

class OrdersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    protected $data;
    protected $user;

    public function __construct()
    {
        $this->user = Auth::guard('admin')->user();
    }

    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', 'Admin.orders.action')
            ->addColumn('collection_date', function ($order) {
                if ($order->collection_date) {
                    return Carbon::parse($order->collection_date)->format('d-m-Y');
                } else {
                    return '';
                }
            })
            ->addColumn('status', function ($order) {
                switch ($order->status){
                    case 0:
                        return 'Prenotazione spedizione reicevuta';
                    case 1:
                        return 'Ritirato presso il cliente';
                    case 2:
                        return 'In transito';
                    case 3:
                        return 'Tentativo di consegna 1';
                    case 4:
                        return 'Tentativo di consegna 2';
                    case 5:
                        return 'Consegnato all\'ufficio postale di zona';
                }
            })
            ->escapeColumns([]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Order $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Order $model)
    {
        $model = $model->newQuery();
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $search = "Search: "; // We can also use variables; This is for instruction purpose only
        $page_length = 10; // We can make it dynamic dependent on User
        $row_text = "";
        $need_input_columns = "[0,1]"; // We have to make the array as string to pass it because of array is is needed as string
        $builder = $this->builder();
        $suser_last_login = '';
        if (isset($this->data['suser_last_login'])) {
            $suser_last_login = $this->data['suser_last_login'];
        }
        $builder->postAjax([
            'url' => route('admin.order.index'),
            'data' => "function(d) { d.suser_last_login = '$suser_last_login'; }",
        ]);
        return $builder
            ->setTableId('user-table')
            ->columns($this->getColumns())
            ->dom('<"row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>rt<"row"<"col-sm-12 col-md-5"i><"col-sm-12 col-md-7"p>><"clear">')
            ->orderBy(1, 'asc')
            ->buttons(
                Button::make('create'),
                Button::make('export'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload')
            )
            ->parameters(array(
                'language' => array(
                    'lengthMenu' => '_MENU_ records',
                    'search' => $search,
                    'info' => 'Showing _START_ to _END_ of _TOTAL_ records',
                ),
                'lengthMenu' => array(
                    array(5, 10, 25, 50, 100, -1),
                    array('5' . $row_text, '10' . $row_text, '25' . $row_text, '50' . $row_text, '100' . $row_text, 'Show all')
                ),
                'pagingType' => "full_numbers",
                'pageLength' => $page_length,
                'createdRow' => "function (row, data, dataIndex ) {
                    $(row).attr('id', 'tr-' + data.id);
                }",
                'searchPlaceholder' => "Search...",
//                'initComplete' => "function () {
//                    this.api().columns($need_input_columns).every(function (colIdx) {
//                        var column = this;
//                        var title = $('tfoot').find('th').eq(colIdx).text();
//                        console.log($(column.footer()).empty());
//                        var input = document.createElement('input');
//                        // input.setAttribute('type', 'text');
//                        input.placeholder = title;
//                        $(input).appendTo($(column.footer()).empty())
//                        .on('change keyup clear', function () {
//                             column.search($(this).val(), false, false,true).draw();
//                        });
//                    });
//                }",
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('order_code', 'order_code')->title('Order Code')->footer('Order Code'),
            Column::make('sender_full_name', 'sender_full_name')->title('Sender Full Name')->footer('Sender Full Name'),
            Column::make('sender_email', 'sender_email')->title('Sender Email')->footer('Sender Email'),
            Column::make('sender_phone', 'sender_phone')->title('Sender Phone')->footer('Sender Phone'),
            Column::make('sender_company_name', 'sender_company_name')->title('Sender Company')->footer('Sender Company'),
            Column::make('collection_date', 'collection_date')->title('Collection Date')->footer('Collection Date'),
            Column::make('sender_city', 'sender_city')->title('Sender City')->footer('Sender City'),
            Column::make('status', 'status')->title('Status')->footer('Status'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(180)
                ->addClass('text-center')
                ->footer('Actions'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'User_' . date('YmdHis');
    }

    public function setData($dataArray)
    {
        $this->data = $dataArray;
        return $this;
    }
}
