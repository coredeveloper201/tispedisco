<?php 

namespace App\Services\Fedex;

use App\Carrier;

use FedEx\ShipService;
use FedEx\ShipService\ComplexType;
use FedEx\ShipService\SimpleType;

class Shipment
{
	public static function getFedexConfig()
	{
    	$carrier = Carrier::where('title', 'Fedex')->first();

        return json_decode($carrier->configs);
	}


	public static function createShipment($config, $shippingData)
	{

    	$config = self::getFedexConfig();
    	if (!isset($shippingData)) {
    		return [
    			'status' => false,
    			'message' => 'No shipping data found, please provide shipping data to create shipping request.'
    		];
    	}

    	$config = self::getFedexConfig();
    	if (!isset($config)) {
    		return [
    			'status' => false,
    			'message' => 'No configuration found for FedEx API.'
    		];
    	}

		$userCredential = new ComplexType\WebAuthenticationCredential();
		$userCredential
		    ->setKey($config->app_key)
		    ->setPassword($config->password);

		$webAuthenticationDetail = new ComplexType\WebAuthenticationDetail();
		$webAuthenticationDetail->setUserCredential($userCredential);

		$clientDetail = new ComplexType\ClientDetail();
		$clientDetail
		    ->setAccountNumber($config->account_number)
		    ->setMeterNumber($config->meter_number);

		$version = new ComplexType\VersionId();
		$version
		    ->setMajor(23)
		    ->setIntermediate(0)
		    ->setMinor(0)
		    ->setServiceId('ship');

		$shipperAddress = new ComplexType\Address();
		$shipperAddress
		    ->setStreetLines($shippingData['sender']['StreetLines'])
		    ->setCity($shippingData['sender']['City'])
		    ->setStateOrProvinceCode($shippingData['sender']['StateOrProvinceCode'])
		    ->setPostalCode($shippingData['sender']['PostalCode'])
		    ->setCountryCode($shippingData['sender']['CountryCode']);

		$shipperContact = new ComplexType\Contact();
		$shipperContact
		    ->setCompanyName($shippingData['sender']['CompanyName'])
		    ->setEMailAddress($shippingData['sender']['EMailAddress'])
		    ->setPersonName($shippingData['sender']['PersonName'])
		    ->setPhoneNumber($shippingData['sender']['PhoneNumber']);


		$shipper = new ComplexType\Party();
		$shipper
		    ->setAccountNumber($config->account_number)
		    ->setAddress($shipperAddress)
		    ->setContact($shipperContact);


		$recipientAddress = new ComplexType\Address();
		$recipientAddress
		    ->setStreetLines($shippingData['recipient']['StreetLines'])
		    ->setCity($shippingData['recipient']['City'])
		    ->setStateOrProvinceCode($shippingData['recipient']['StateOrProvinceCode'])
		    ->setPostalCode($shippingData['recipient']['PostalCode'])
		    ->setCountryCode($shippingData['recipient']['CountryCode']);

		$recipientContact = new ComplexType\Contact();
		$recipientContact
		    ->setPersonName($shippingData['recipient']['PersonName'])
		    ->setPhoneNumber($shippingData['recipient']['PhoneNumber']);

		$recipient = new ComplexType\Party();
		$recipient
		    ->setAddress($recipientAddress)
		    ->setContact($recipientContact);



		$labelSpecification = new ComplexType\LabelSpecification();
		$labelSpecification
		    ->setLabelStockType(new SimpleType\LabelStockType(SimpleType\LabelStockType::_PAPER_7X4POINT75))
		    ->setImageType(new SimpleType\ShippingDocumentImageType(SimpleType\ShippingDocumentImageType::_PDF))
		    ->setLabelFormatType(new SimpleType\LabelFormatType(SimpleType\LabelFormatType::_COMMON2D));




		$shippingChargesPayor = new ComplexType\Payor();
		$shippingChargesPayor->setResponsibleParty($shipper);

		$shippingChargesPayment = new ComplexType\Payment();
		$shippingChargesPayment
		    ->setPaymentType(SimpleType\PaymentType::_SENDER)
		    ->setPayor($shippingChargesPayor);



		$requestedShipment = new ComplexType\RequestedShipment();
		$requestedShipment->setShipTimestamp($shippingData['expectedTimestamp']);

		$requestedShipment->setDropoffType(new SimpleType\DropoffType(SimpleType\DropoffType::_REGULAR_PICKUP));
		$requestedShipment->setServiceType(new SimpleType\ServiceType($shippingData['ServiceType']));
		$requestedShipment->setPackagingType(new SimpleType\PackagingType($shippingData['PackagingType']));
		
		$requestedShipment->setShipper($shipper);
		$requestedShipment->setRecipient($recipient);

		$requestedShipment->setLabelSpecification($labelSpecification);

		$requestedShipment->setRateRequestTypes(array(new SimpleType\RateRequestType(SimpleType\RateRequestType::_PREFERRED)));




		$CustomsClearanceDetail = [
			'DutiesPayment' => new ComplexType\Payment([
				'PaymentType' => 'SENDER',
					'Payor' => new ComplexType\Payor([
					'ResponsibleParty' => new ComplexType\Party([
						'AccountNumber' => $config->account_number,
						'Contact' => $shipperContact,
						'Address' => $shipperAddress
					])  
				])
			]),
			'Commodities' => [
				[
					'NumberOfPieces' => 1,
					'Description' => 'Books',
					'CountryOfManufacture' => 'MX',
					'Weight' => array('Units' => 'LB', 'Value' => 1.0),
					'Quantity' => 4,
					'QuantityUnits' => 'EA',
					'UnitPrice' => array('Currency' => 'USD', 'Amount' => 100.00),
					'CustomsValue' => array('Currency' => 'USD', 'Amount' => 400.00 )
				]
			],
			'DocumentContent' => 'NON_DOCUMENTS',
			'CustomsValue' => new ComplexType\Money([
				'Currency' => 'USD',
				'Amount' => 400.0
			]),
			// 'ExportDetail' => new ComplexType\ExportDetail([
			// 	'ExportComplianceStatement' => 30.37,
			// 	'B13AFilingOption' => 'NOT_REQUIRED'
			// ])
		];

		$requestedShipment->setCustomsClearanceDetail(new ComplexType\CustomsClearanceDetail($CustomsClearanceDetail));


		$packageLineItem1 = new ComplexType\RequestedPackageLineItem();
		$packageLineItem1
		    ->setSequenceNumber(1)
		    ->setItemDescription('Product description')
		    ->setDimensions(new ComplexType\Dimensions(array(
		        'Width' => 10,
		        'Height' => 10,
		        'Length' => 25,
		        'Units' => SimpleType\LinearUnits::_IN
		    )))
		    ->setWeight(new ComplexType\Weight(array(
		        'Value' => 1,
		        'Units' => SimpleType\WeightUnits::_LB
		    )));

		$requestedShipment->setPackageCount(1);
		$requestedShipment->setRequestedPackageLineItems([
		    $packageLineItem1
		]);

		$requestedShipment->setShippingChargesPayment($shippingChargesPayment);

		$processShipmentRequest = new ComplexType\ProcessShipmentRequest();
		$processShipmentRequest->setWebAuthenticationDetail($webAuthenticationDetail);
		$processShipmentRequest->setClientDetail($clientDetail);
		$processShipmentRequest->setVersion($version);
		$processShipmentRequest->setRequestedShipment($requestedShipment);

		$shipService = new ShipService\Request();

		if (config('env') == 'production') {
			$shipService->getSoapClient()->__setLocation('https://ws.fedex.com:443/web-services/ship');
		}

		$result = $shipService->getProcessShipmentReply($processShipmentRequest);

		return response()->download($result->CompletedShipmentDetail->CompletedPackageDetails[0]->Label->Parts[0]->Image);
		return [
			'status' => true,
			'data' => $result->CompletedShipmentDetail->CompletedPackageDetails[0]->Label->Parts[0]->Image,
		];

		// Save .pdf label
		// file_put_contents('/path/to/label.pdf', $result->CompletedShipmentDetail->CompletedPackageDetails[0]->Label->Parts[0]->Image);
		// var_dump($result->CompletedShipmentDetail->CompletedPackageDetails[0]->Label->Parts[0]->Image);

	}

}