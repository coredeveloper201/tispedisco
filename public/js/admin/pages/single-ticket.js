let vm = new Vue({
    el: '#singleTicketVue',
    data: {
        activity: 'list', // list, create, single
        ticket: '',
        tickets: [],
        innerReplyBoxes: [],
        showTicket: '',
        rangeValue: 0,
        reply: ''
    },
    methods: {
        moment(time) {
            return moment(time).locale("it");
        },
        activityChange(activity = 'list') {
            this.getAllTickets();
            this.$set(this, 'activity', activity);
        },
        showComment(replyIndex) {
            var computedReplies = this.innerReplyBoxes;
            computedReplies[replyIndex]['show'] = true;
            this.$set(this, 'innerReplyBoxes', []);
            this.$set(this, 'innerReplyBoxes', computedReplies);
        },
        setData(ticket) {
            var replies = ticket.replies;
            this.innerReplyBoxes = [];
            for (let index = 0; index < replies.length; index++) {
                const reply = replies[index];
                const replyBox = {
                    ticketId: ticket.id,
                    replyId: reply.id,
                    message: '',
                    file: '',
                    replyOn: 'reply',
                    show: false
                }
                this.innerReplyBoxes[index] = replyBox;
            }
            this.$set(this.reply, 'ticketId', ticket.id);
            this.$set(this, 'showTicket', ticket);
        },
        handleFileUpload(event, uploadOn = 'ticket') {
            var that = this;
            var ticketFile = event.target.files[0];
            let formData = new FormData();
            formData.append('file', ticketFile);
            var config = {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
            axios.post(fileUploadRoute, formData, config)
                .then(function (response) {
                    if (!response.data.success) return;
                    switch (uploadOn) {
                        case 'ticket':
                            that.$set(that.ticket, 'file', response.data.url);
                            break;
                        case 'reply':
                            that.$set(that.reply, 'file', response.data.url);
                            break;
                        case 'inner':
                            that.ticket.file = response.data.url;
                            break;
                        default:
                            break;
                    }
                })
        },
        getAllTickets() {
            var that = this;
            axios.get(getTicketsRoute)
                .then(function (response) {
                    that.tickets = response.data.tickets;
                })
        },
        getSingleTicket(ticketId) {
            var self = this;
            axios.get(singleTicketRoute + ticketId)
                .then(function (response) {
                    if (!response.data.success) return;
                    self.setData(response.data.ticket);
                })
        },
        ticketCru(action = 'create') {
            var that = this;
            var formData = {
                action: action,
                ticket: this.ticket
            };
            axios.post(cruTicketRoute, formData)
                .then(function (response) {
                    if (!response.data.success) return;
                    switch (action) {
                        case 'create':
                            that.tickets.push(response.data.ticket);
                            that.$set(that, 'activity', 'list');
                            that.clear();
                            swal({
                                icon: 'success',
                                title: 'Created',
                                text: 'Successfully created a new ticket!',
                                timer: 1000
                            });
                            break;
                    }
                })
        },
        replyCru(reply) {
            var that = this;
            var formData = {
                reply: reply
            };
            axios.post(cruReplyRoute, formData)
                .then(function (response) {
                    if (!response.data.success) return;
                    switch (reply.replyOn) {
                        case 'ticket':
                            swal({
                                icon: 'success',
                                title: 'Created',
                                text: 'Successfully created a new ticket!',
                                timer: 1000
                            })
                            break;
                        case 'reply':
                            swal({
                                icon: 'success',
                                title: 'Created',
                                text: 'Successfully created a new ticket!',
                                timer: 1000
                            })
                            break;
                    }
                    that.$set(that.reply, 'message', '');
                    that.$set(that.reply, 'file', '');
                    that.getSingleTicket(reply.ticketId);
                })
        },

        clear() {
            this.ticket = {
                id: '',
                title: '',
                message: '',
                file: '',
                is_paralyzes: false
            }
            this.reply = {
                ticketId: '',
                message: '',
                file: '',
                replyOn: 'ticket',
                show: true
            }
        }
    },
    mounted() {
        this.clear();
        this.getSingleTicket(ticketId);
    },
});
