@extends('Admin.layouts.app')

@section('custom-css')
@endsection

@section('page-title')
    {{ __('translations.User') }} | {{ $user->first_name }} {{ $user->last_name }}
@endsection

@push('cssLib')
@endpush

@push('css')
@endpush

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="">{{ __('translations.Home') }}</a></li>
@endsection

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <h4 class="card-title">{{ __('translations.Customer Info Update') }}</h4>
                        <h6 class="card-subtitle"></h6>
                        <form action="{{ route('admin.users.update', $user->id) }}" method="post">
                            @csrf
                            @method('PUT')
                            <div class="form-group mt-5 row">
                                <label for="first_name" class="col-2 col-form-label">{{ __('translations.First Name') }}</label>
                                <div class="col-10">
                                    <input class="form-control" type="text" name="first_name" value="{{ old('first_name', $user->first_name) }}" id="first_name">
                                </div>
                            </div>

                            <div class="form-group mt-5 row">
                                <label for="last_name" class="col-2 col-form-label">{{ __('translations.Last Name') }}</label>
                                <div class="col-10">
                                    <input class="form-control" type="text" name="last_name" value="{{ old('last_name', $user->last_name) }}" id="last_name" placeholder="Last Name">
                                </div>
                            </div>

                            <div class="form-group mt-5 row">
                                <label for="email" class="col-2 col-form-label">{{ __('translations.Email') }}</label>
                                <div class="col-10">
                                    <input class="form-control" type="text" name="email" value="{{ old('email', $user->email) }}" id="email" placeholder="Email">
                                </div>
                            </div>

                            <div class="form-group mt-5 row">
                                <div class="col-2 col-form-label"></div>
                                <div class="col-10">
                                    <button type="submit" class="btn btn-success mr-2">{{ __('translations.Update') }}</button>
                                    <a href="{{ route('admin.users.index') }}" class="btn btn-dark mr-2">{{ __('translations.Cancel') }}</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    @include('messages.toastr')
@endpush
