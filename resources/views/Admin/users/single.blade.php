@extends('Admin.layouts.app')

@section('custom-css')
@endsection

@section('page-title')
    {{ __('translations.User View') }}
@endsection

@push('cssLib')
@endpush

@push('css')
@endpush

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="">{{ __('translations.Home') }}</a></li>
@endsection

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <!-- Column -->
            <div class="col-lg-4 col-xlg-3 col-md-5">
                <div class="card">
                    <div class="card-body">
                        <center class="mt-4">
                            @if ($user->avatar)
                                <img src="{{ asset('uploads/users/profile/').'/'.$user->avatar }}" class="rounded-circle"
                                     width="150">
                            @else
                                <img src="{{ asset('template/assets/images/users/5.jpg') }}" class="rounded-circle"
                                     width="150">
                            @endif
                            <h4 class="card-title mt-2">{{ $user->first_name }}  {{ $user->last_name }}</h4>
                            <h6 class="card-subtitle">{{ __('translations.Customer') }}</h6>
                        </center>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-8 col-xlg-9 col-md-7">
                <div class="card">
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade active show" id="last-month" role="tabpanel"
                             aria-labelledby="pills-profile-tab">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 col-xs-6 b-r"><strong>{{ __('translations.Full Name') }}</strong>
                                        <br>
                                        <p class="text-muted">{{ $user->first_name }}  {{ $user->last_name }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"><strong>{{ __('translations.Mobile') }}</strong>
                                        <br>
                                        <p class="text-muted">(123) 456 7890</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"><strong>{{ __('translations.Email') }}</strong>
                                        <br>
                                        <p class="text-muted">{{ $user->email }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"><strong>{{ __('translations.Location') }}</strong>
                                        <br>
                                        <p class="text-muted">{{ __('translations.London') }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
    </div>
@endsection

@push('scripts')
@endpush
