@extends('Admin.layouts.app')

@section('custom-css')
@endsection

@section('page-title')
    Orders
@endsection

@push('cssLib')
    <link rel="stylesheet"
          href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet"
          href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.bootstrap4.min.css">
@endpush

@push('css')
@endpush

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="">{{ __('translations.Dashboard') }}</a></li>
@endsection

@section('content')
    <section id="orderIndexVue">
        <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="changeStatus"
             aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="changeStatus">{{ __('translations.Change Status') }}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <form>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">{{ __('translations.Status') }}:</label>
                                <select class="form-control" v-model="order.status">
                                    <option value="0">{{ __('translations.Prenotazione spedizione reicevuta') }}</option>
                                    <option value="1">{{ __('translations.Ritirato presso il cliente') }}</option>
                                    <option value="2">{{ __('translations.In transito') }}</option>
                                    <option value="3">{{ __('translations.Tentativo di consegna 1') }}</option>
                                    <option value="4">{{ __('translations.Tentativo di consegna 2') }}</option>
                                    <option value="5">{{ __('translations.Consegnato all\'ufficio postale di zona') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('translations.Close') }}</button>
                            <button type="button" class="btn btn-primary" @click="changeStatus" data-dismiss="modal">{{ __('translations.Change') }}</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </section>
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <h4 class="card-title">{{ __('translations.Order List') }}</h4>
                        <h6 class="card-subtitle"></h6>
                        <div class="table-responsive">
                            {!! $dataTable->table(['class' => 'table table-striped border display', 'style' => 'width: 100%;'],  true) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.bootstrap4.min.js"></script>

    <script src="{{ asset('vendor/datatables/buttons.server-side.js') }}" type="text/javascript"></script>
    {!! $dataTable->scripts() !!}

    <script type="text/javascript">
        function Delete(id) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    axios.delete('{{url()->current()}}/' + id, {}).then(function (response) {
                        if (response.data.success) {
                            $('#tr-' + id).fadeOut();
                            Swal.fire(
                                'Deleted!',
                                'Your order has been deleted.',
                                'success'
                            )
                        }
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            });
        }

        let singleOrderRoute = '{{ route('api.order.index') }}';
        let changeOrderStatus = '{{ route('admin.order.change-status') }}';
    </script>
    <script src="{{ asset('js/admin/pages/order-index.js') }}"></script>
@endpush
