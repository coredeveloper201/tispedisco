let vm = new Vue({
    el: '#orderIndexVue',
    data: {
        order: {},
    },
    methods: {
        moment(time) {
            return moment(time).locale("it");
        },
        clear() {
            this.order = {};
        },
        getSingleOrder(orderId) {
            let self = this;
            axios.get(singleOrderRoute + '/' + orderId)
                .then(function (response) {
                    if (!response.data.success) return;
                    self.order = response.data.order;
                })
        },
        changeStatus() {
            axios.post(changeOrderStatus, {
                id: this.order.id,
                status: this.order.status
            }).then(function (response) {
                if (response.data.success) {
                    $('table').DataTable().ajax.reload(null, false);
                    toastr.success(response.data.message);
                } else {
                    toastr.error(response.data.message);
                }
            }).catch(function (error) {
                console.log(error);
                toastr.error(error.data.message);
            });
        }
    },
    mounted() {
        this.clear();
    },
});
