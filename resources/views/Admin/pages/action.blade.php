<a class="btn waves-effect waves-light btn-rounded btn-xs btn-success" href="{{ route('admin.pages.edit', $id) }}"><i
        class="fa fa-edit"></i></a>
<button class="btn waves-effect waves-light btn-rounded btn-xs btn-danger" onclick="Delete('{{$id}}')"><i
        class="fa fa-trash"></i></button>
