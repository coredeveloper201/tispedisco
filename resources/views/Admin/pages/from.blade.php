@extends('Admin.layouts.app')

@section('custom-css')
@endsection

@section('page-title')
    @if (isset($page->id))
        {{ __('translations.Edit Pages') }}
    @else
        {{ __('translations.Create Page') }}
    @endif
@endsection

@push('cssLib')
@endpush

@push('css')
    <link rel="stylesheet" href="{{asset('/template/assets/libs/summernote/dist/summernote-bs4.css')}}">
@endpush

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="">{{ __('translations.Dashboard') }}</a></li>
@endsection

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        @if (isset($page->id))
                            <h4 class="card-title">{{ __('translations.Edit Page') }}</h4>
                        @else
                            <h4 class="card-title">{{ __('translations.Create Page') }}</h4>
                        @endif
                        <h6 class="card-subtitle"></h6>
                        <form
                            @if (isset($page->id))
                            action="{{ route('admin.pages.update', $page->id) }}"
                            @else
                            action="{{ route('admin.pages.store') }}"
                            @endif
                            method="post">
                            @csrf
                            @if (isset($page->id))
                                @method('PUT')
                            @endif
                            <div class="form-group">
                                <label for="page_title">{{ __('translations.Page Title') }}</label>
                                <input type="text" class="form-control" name="page_title" id="page_title"
                                       value="{{ old('page_title', $page->page_title) }}"
                                       placeholder="Page Title">
                            </div>
                            <div class="form-group">
                                <label for="slug">{{ __('translations.Page Slug') }}</label>
                                <input type="text" class="form-control" name="slug" id="slug"
                                       value="{{ old('page_title', $page->slug) }}"
                                       placeholder="Page Slug">
                            </div>
                            <div class="form-group">
                                <label for="meta_title">{{ __('translations.Meta Title') }}</label>
                                <input type="text" class="form-control" name="meta_title" id="meta_title"
                                       value="{{ old('meta_title', $page->meta_title) }}"
                                       placeholder="Meta Title">
                            </div>
                            <div class="form-group">
                                <label for="meta_title">{{ __('translations.Meta Description') }}</label>
                                <textarea class="form-control" name="meta_description" id="meta_description"
                                          cols="30"
                                          rows="10">{{ old('meta_description', $page->meta_description) }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="title">{{ __('translations.Page Content') }}</label>
                                <textarea class="summernote"
                                          name="content">{!! old('content', $page->content) !!}</textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn waves-effect waves-light btn-primary">
                                    {{ __('translations.Save') }}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{asset('/template/assets/libs/summernote/dist/summernote-bs4.min.js')}}"></script>
    <script !src="">
        $(document).ready(function () {
            $('.summernote').summernote({
                height: 500
            });
        });
    </script>
@endpush
