@extends('Admin.layouts.app')

@section('custom-css')
@endsection

@section('page-title')
    {{ __('translations.Carrier API Settings') }}
@endsection

@push('cssLib')
@endpush

@push('css')
@endpush

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="">{{ __('translations.Carrier API Settings') }} - {{ $carrier->title }}</a></li>
@endsection

@section('content')
<div class="page-content container-fluid">
    <form method="post" action="{{ route('admin.carriers.update', $carrier->id) }}" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ Session::token() }}">
        <input type="hidden" name="_method" value="PUT">
        <div class="row">
            <div class="col-md-4">
                <div class="from-group">
                    <label>{{ __('translations.Carrier Title') }}</label>
                    <input class="form-control" type="text" name="title" value="{{ $carrier->title }}" required>
                </div>
            </div>
            <div class="col-md-4">
                <label>{{ __('translations.Carrier Logo') }}</label>
                 <div class="custom-file">
                    <input name="logo" type="file" class="custom-file-input">
                    <label class="custom-file-label">{{ __('translations.Choose file') }}...</label>
                  </div>
            </div>
            <div class="col-md-4">
                <label>{{ __('translations.Carrier Fee') }}(in %)</label>
                <input name="fee" type="number" step="0.01" class="form-control" value="{{ $carrier->fee }}" required>
            </div>
        </div>
        <hr>
        <div class="row text-right">
            <div class="col-12"><button id="addKey" class="btn btn-primary">{{ __('translations.Add New Key') }}</button></div>
        </div>
        <hr>
        <div class="row" id="keys">
            @php $keyCount = 1; @endphp
            @if($carrier->configs)
                @foreach(json_decode($carrier->configs) as $key => $value)
                    @php $keyCount += 1; @endphp
                    <div class="col-md-3 mt-4" id="key{{ $loop->iteration }}">
                        <div class="row">
                            <div class="col-6">
                                <label>Key {{ $loop->iteration }}</label>
                            </div>
                            <div class="col-6">
                                <button type="button" data-no="{{ $loop->iteration }}" style="padding: 2px 5px;" class="btn btn-danger pull-right removeKey"><i class="fa fa-trash-alt"></i></button>
                            </div>
                        </div>
                        <div class="from-group">
                            <input class="form-control" type="text" name="configs[{{ $loop->iteration }}][name]" value="{{ $key }}" placeholder="Enter key name">
                        </div>
                        <div class="from-group mt-2">
                            <input class="form-control" type="text" name="configs[{{ $loop->iteration }}][value]" value="{{ $value }}" placeholder="Enter key value">
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
        <div class="row mt-3">
             <div class="col-12">
                 <button class="btn btn-success">{{ __('translations.Save Settings') }}</button>
             </div>
         </div> 
    </form>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        let keys = {{ $keyCount }};
        $("#addKey").click(function(e) {
            e.preventDefault();
            $("#keys").append(`<div class="col-md-3 mt-4" id="key${keys}">
                    <div class="row">
                        <div class="col-6">
                            <label>Key ${keys}</label>
                        </div>
                        <div class="col-6">
                            <button type="button" data-no="${keys}" style="padding: 2px 5px;" class="btn btn-danger pull-right removeKey"><i class="fa fa-trash-alt"></i></button>
                        </div>
                    </div>
                    <div class="from-group">
                        <input class="form-control" type="text" name="configs[${keys}][name]" value="" placeholder="Enter key name" required>
                    </div>
                    <div class="from-group mt-2">
                        <input class="form-control" type="text" name="configs[${keys}][value]" value="" placeholder="Enter key value" required>
                    </div>
                </div>`);
            keys++;
        });

        $("#keys").on('click', '.removeKey', function () {
            console.log($(this).attr('data-no'));
            $("#key" + $(this).attr('data-no')).remove();
        });
    });
</script>
@endpush
