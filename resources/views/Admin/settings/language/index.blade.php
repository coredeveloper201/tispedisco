@extends('Admin.layouts.app')

@section('custom-css')
@endsection

@section('page-title')
    Languages
@endsection

@push('cssLib')

@endpush

@push('css')
@endpush

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="">Languages</a></li>
@endsection

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6">
                                <h4 class="card-title">All Languages</h4>
                            </div>
                            <div class="col-6">
                                <a href="{{ route('admin.languages.create') }}" class="btn btn-primary float-right mb-3">Add New Language</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

@endpush
