@extends('Admin.layouts.app')

@section('custom-css')
@endsection

@section('page-title')
    {{ __('translations.Dashboard') }}
@endsection

@push('cssLib')
@endpush

@push('css')
@endpush

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="">{{ __('translations.Dashboard') }}</a></li>
@endsection

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-group">
                            <div class="card p-2 p-lg-3">
                                <div class="p-lg-3 p-2">
                                    <div class="d-flex align-items-center">
                                        <button class="btn btn-circle btn-primary text-white btn-lg"
                                                href="javascript:void(0)">
                                            <i class="mdi mdi-account-multiple"></i>
                                        </button>
                                        <div class="ml-4" style="width: 38%;">
                                            <h4 class="font-light">{{ __('translations.Total Customers') }}</h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-danger" role="progressbar"
                                                     style="width: 100%" aria-valuenow="40" aria-valuemin="0"
                                                     aria-valuemax="40"></div>
                                            </div>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="display-7 mb-0">{{ $data['total_users'] }}</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card p-2 p-lg-3">
                                <div class="p-lg-3 p-2">
                                    <div class="d-flex align-items-center">
                                        <button class="btn btn-circle btn-cyan text-white btn-lg"
                                                href="javascript:void(0)">
                                            <i class="ti-wallet"></i>
                                        </button>
                                        <div class="ml-4" style="width: 38%;">
                                            <h4 class="font-light">{{ __('translations.Total Orders') }}</h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-cyan" role="progressbar" style="width: 100%"
                                                     aria-valuenow="40" aria-valuemin="0" aria-valuemax="40"></div>
                                            </div>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="display-7 mb-0">{{ $data['total_orders'] }}</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card p-2 p-lg-3">
                                <div class="p-lg-3 p-2">
                                    <div class="d-flex align-items-center">
                                        <button class="btn btn-circle btn-warning text-white btn-lg"
                                                href="javascript:void(0)">
                                            <i class="mdi mdi-sync-alert"></i>
                                        </button>
                                        <div class="ml-4" style="width: 38%;">
                                            <h4 class="font-light">{{ __('translations.Total Tickets') }}</h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-warning" role="progressbar"
                                                     style="width: 100%" aria-valuenow="40" aria-valuemin="0"
                                                     aria-valuemax="40"></div>
                                            </div>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="display-7 mb-0">{{ $data['total_tickets'] }}</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
@endpush
