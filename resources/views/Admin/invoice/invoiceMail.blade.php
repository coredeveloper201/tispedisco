{{ __('translations.Order Code') }}: {{ $order->order_code }} <br>
{{ __('translations.Collection Date') }}: {{ $order->collection_date }} <br>
{{ __('translations.Order Status') }}:
@switch($order->status)
    @case(0)
    {{ __('translations.Prenotazione spedizione reicevuta') }}
    @break

    @case(1)
    {{ __('translations.Ritirato presso il cliente') }}
    @break

    @case(2)
    {{ __('translations.In transito') }}
    @break

    @case(3)
    {{ __('translations.Tentativo di consegna 1') }}
    @break

    @case(4)
    {{ __('translations.Tentativo di consegna 2') }}
    @break

    @case(5)
    {{ __('translations.Consegnato all\'ufficio postale di zona') }}
    @break

    @default
    {{ __('translations.Undefined') }}
@endswitch
<br>
