@extends('Admin.layouts.app')

@section('custom-css')
@endsection

@section('page-title')
    {{ __('translations.Tickets') }}
@endsection

@push('cssLib')
    <link rel="stylesheet"
          href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet"
          href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.bootstrap4.min.css">
@endpush

@push('css')
@endpush

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="">{{ __('translations.Dashboard') }}</a></li>
@endsection

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <h4 class="card-title">{{ __('translations.Order List') }}</h4>
                        <h6 class="card-subtitle"></h6>
                        <div class="table-responsive">
                            {!! $dataTable->table(['class' => 'table table-striped border display', 'style' => 'width: 100%;'],  true) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.bootstrap4.min.js"></script>

    <script src="{{ asset('vendor/datatables/buttons.server-side.js') }}" type="text/javascript"></script>
    {!! $dataTable->scripts() !!}

    <script>
        function Delete(id) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    axios.delete('{{url()->current()}}/' + id, {}).then(function (response) {
                        if (response.data.success) {
                            $('#tr-' + id).fadeOut();
                            Swal.fire(
                                'Deleted!',
                                'Your order has been deleted.',
                                'success'
                            )
                        }
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            });
        }

        function changeStatus(id, currentStatus) {
            axios.post('{{ route('admin.order.change-status') }}', {
                id: id,
                status: currentStatus
            }).then(function (response) {
                if (response.data.success) {
                    $('table').DataTable().ajax.reload(null, false);
                    toastr.success(response.data.message);
                }else {
                    toastr.error(response.data.message);
                }
            }).catch(function (error) {
                console.log(error);
                toastr.error(error.data.message);
            });
        }
    </script>
@endpush
