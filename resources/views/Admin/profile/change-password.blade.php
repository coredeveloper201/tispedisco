@extends('Admin.layouts.app')

@section('custom-css')
@endsection

@section('page-title')
    {{ __('translations.Change Password') }}
@endsection

@push('cssLib')
@endpush

@push('css')
@endpush

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="">{{ __('translations.Dashboard') }}</a></li>
@endsection

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <!-- Column -->
            <div class="col-lg-6 col-xlg-6 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <form class="form-horizontal form-material" action="{{ route('admin.profile.changePassword') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label class="col-md-12">{{ __('translations.Old Password') }}</label>
                                <div class="col-md-12">
                                    <input type="password" value="{{ old('current_password') }}" class="form-control form-control-line" name="current_password" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">{{ __('translations.New Password') }}</label>
                                <div class="col-md-12">
                                    <input type="password" value="{{ old('password') }}" class="form-control form-control-line" name="password" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">{{ __('translations.Confirm Password') }}</label>
                                <div class="col-md-12">
                                    <input type="password" value="{{ old('password_confirmation') }}" class="form-control form-control-line" name="password_confirmation" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-success">{{ __('translations.Update Password') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
@endpush
