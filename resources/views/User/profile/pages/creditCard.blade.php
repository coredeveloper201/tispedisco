@extends('User.profile.profileMaster')

@section('userContent')
    <div id="creditCardVue">
        <div class="state-credit">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12" v-for="card in cards">
                    <div class="address-box-wrapper">
                        <div class="address-box">
                            <div class="credit-info">
                                <div class="sec-logo">
                                    <i class="mdi mdi-lg  mdi-credit-card"></i>
                                </div>
                                <div class="sec-number"><span>**** **** **** @{{ card.last4 }}</span></div>
                            </div>
                            <div class="add-text">{{ __('translations.Intestata a') }}:</div>
                            <div class="add-text">{{ auth()->user()->first_name .' '. auth()->user()->last_name }}</div>
                            <div data-toggle="modal" data-target="#editCreditCard" class="text-right c-pointer"><span
                                    class="text-green text-sm " @click="getSingleCard(card.id)">{{ __('translations.Modifica') }}</span> &nbsp;<i
                                    class="text-green mdi-sm  mdi mdi-wrench"></i></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- /.modal-edit-credit -->
        <div class="modal fade" id="editCreditCard" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header modal-header-custom">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h5 class="modal-title"><strong>{{ __('translations.Modifica Carta di pagamento') }}</strong></h5>
                    </div>
                    <div class="modal-body modal-header-custom">
                        <p>Utilizza questo form per modificare la carta di credito, ricordati di cliccare sul botte
                            "Salva"
                            al termine. Verrà effettuata addebitata una piccola somma per verifica, la quale verrà
                            immediatamente stornata ad esito positivo.</p>
                        <div class="form-group position-relative margin-btm-input-lg">
                            <div class="mb-1">
                                <label for="">{{ __('translations.Numero carta di credito') }}</label>
                                <input type="text" class="form-control input-gray profile-input"
                                       placeholder="0000 0000 0000 0000"
                                       v-model="updateCard.last4">
                                <img class="input-img" src="{{asset('images/home-img/cards.png')}}" alt="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group margin-btm-input-lg">
                                    <div class="mb-1">
                                        <label for="">{{ __('translations.Data di scadenza') }}</label>
                                        <input type="text" class="form-control input-gray profile-input"
                                               placeholder="00 / 00"
                                               v-model="updateCard.card_exp">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group margin-btm-input-lg">
                                    <div class="mb-1">
                                        <label for="">CVV</label>
                                        <input type="text" class="form-control input-gray profile-input"
                                               placeholder="000">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer modal-footer-custom">
                        <button type="button" class="btn btn-close" data-dismiss="modal">{{ __('translations.Non salvare') }}</button>
                        <button class="btn btn-success" @click="updateCardInfo(updateCard.id)" data-dismiss="modal">{{ __('translations.Salva modifiche') }}</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
@endsection


@push('scripts')

    <script type="text/javascript">
        new Vue({
            el: '#creditCardVue',
            data: {
                cards: [],
                singleCard: {},
                updateCard: {
                    card_exp: null,
                    last4: null,
                },
            },
            methods: {
                clear() {
                },
                getCards() {
                    let self = this;
                    axios.get("{{route('user.getCards')}}")
                        .then(function (response) {
                            if (!response.data.success) return;
                            self.cards = response.data.cards.data;
                        })
                },
                getSingleCard(cardId) {
                    let self = this;
                    axios.post("{{route('user.getSingleCard')}}", {
                        cardId: cardId
                    }).then(function (response) {
                        console.log(response);
                        if (!response.data.success) return;
                        self.singleCard = response.data.card;
                        self.updateCard.id = self.singleCard.id;
                        self.updateCard.card_exp = self.singleCard.exp_month + '/' + self.singleCard.exp_year;
                        self.updateCard.last4 = '**** **** **** ' + self.singleCard.last4;
                    })
                },
                updateCardInfo(cardId) {
                    let self = this;
                    axios.post("{{route('user.updateCard')}}", {
                        cardId: cardId,
                        card_exp: self.updateCard.card_exp,
                    }).then(function (response) {
                        console.log(response);
                        if (!response.data.success) return;
                        self.singleCard = response.data.card;
                    })
                }
            },
            mounted() {
                this.clear();
                this.getCards();
            },
        });
    </script>
@endpush
