<header class="header-user">
    <div class="container">
        <section class="navigation-bar">
            <nav class="navbar navbar-default nav-transparent nav-green nav-padding">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand brand-custom" href="{{ route('home') }}"><img src="{{asset('images/home-img/w-logo.png')}}"
                                                                           alt=""></a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="#">{{ __('translations.Spedisci') }}</a></li>

                            @guest
                            <li><a href="#">{{ __('translations.Traccia') }}</a></li>
                            <li><a href="#">{{ __('translations.Registrati') }}</a></li>
                            @endguest
                            <li class="dropdown">
                                    <a href="#" style="background-color: transparent; color: #ffffff" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="mdi mdi-md mdi-translate"></i>
                                        {{ config('languages.' . App::getLocale()) }}
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" style="background-color: rgb(72, 139, 94);">
                                        @foreach(config('languages') as $key => $name)
                                            <li><a href="{{ route('changeLanguage', $key) }}">{{ $name }}</a></li>
                                        @endforeach
                                    </ul>
                                </li>
                        </ul>
                        @if(Auth::check())
                            <div class="navbar-form navbar-right">
                                <a class="btn btn-login-user" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                    {{ __('translations.Esci') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        @endif
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </section>
    </div>

</header>

<style type="text/css">
    .dropdown-menu > li > a:hover{
        background: #386f4a !important;
    }
</style>
