  <?php

  return array (
  'Accedi al tuo account'                                                             => 'Log in to your account',
  'Accedie'                                                                           => 'Accedie',
  'Accetto la privacy policy del sito web'                                            => 'I accept the website privacy policy',
  'Accetto le condizioni di vendita espresse nei termini e condizioni del sito web'   => 'I accept the conditions of sale expressed in the terms and conditions of the website',
  'Add New Carrier'                                                                   => 'Add New Carrier',
  'Add New Key'                                                                       => 'Add New Key',
  'Add New Page'                                                                      => 'Add New Page',
  'Add new carrier settings'                                                          => 'Add new carrier settings',
  'Aiuto'                                                                             => 'Help',
  'All Rights Reserved'                                                               => 'All Rights Reserved',
  'Altezza'                                                                           => 'Height',
  'Assegna un nome alla spedizione'                                                   => 'Name your shipment',
  'Assicurazione'                                                                     => 'Insurance',
  'Calcola il costo della spedizione'                                                 => 'Calculate the cost of shipping',
  'Calcola il tuo preventivo'                                                         => 'Calculate your estimate',
  'Calcola preventivo'                                                                => 'Calculate quote',
  'Calcolo della spedizione'                                                          => 'Shipping calculation',
  'Cancel'                                                                            => 'Cancel',
  'Cap'                                                                               => 'Postal Code',
  'Carrier API Settings'                                                              => 'Carrier API Settings',
  'Carrier Logo'                                                                      => 'Carrier Logo',
  'Carrier Title'                                                                     => 'Carrier Title',
  'Cerca il luogo di destinazione'                                                    => 'Search for the place of destination',
  'Cerca il luogo di partenza'                                                        => 'Search for the place of departure',
  'Change'                                                                            => 'Change',
  'Change Password'                                                                   => 'Change Password',
  'Change Status'                                                                     => 'Change Status',
  'Chi siamo'                                                                         => 'Who we are',
  'Choose file'                                                                       => 'Choose file',
  'Città'                                                                             => 'City',
  'Close'                                                                             => 'Close',
  'Codice SDI'                                                                        => 'SDI code',
  'Codice di interscambio'                                                            => 'Interchange code',
  'Cognome'                                                                           => 'Surname',
  'Collection Date'                                                                   => 'Collection Date',
  'Collega il tuo ecommerce'                                                          => 'Connect your ecommerce',
  'Come funziona TiSpedisco'                                                          => 'How TiSpedisco works',
  'Condividi questo preventivo'                                                       => 'Share this quote',
  'Conferma pagamento'                                                                => 'Confirm payment',
  'Conferma prenotazione spedizione'                                                  => 'Confirm shipment booking',
  'Confermiamo la prenotazione per il ritiro della spezione'                          => 'We confirm the booking for the collection of the shipment',
  'Confirm Password'                                                                  => 'Confirm Password',
  'Consegna pacchi internazionale'                                                    => 'International parcel delivery',
  'Consegnato a casa o sul luogo di lavoro'                                           => 'Delivered to your home or workplace',
  'Consulta le disposizioni riguardanti le restrizioni'                               => 'See the provisions regarding restrictions',
  'Contenuto'                                                                         => 'Content',
  'Continua'                                                                          => 'Keep on',
  'Convalida la carta'                                                                => 'Validate your card',
  'Copertura assicurativa opzionale disponibile fino a un valore di 1.000,00 €.'      => 'Copertura assicurativa opzionale disponibile fino a un valore di 1.000,00 €.',
  'copyright_text'                                                                    => 'Tutti i testi e la grafica presenti nel sito sono soggetti alle norme vigenti in materia di diritto d"autore.',
  'Cosa dicono di noi'                                                                => 'What they say about us',
  'Costo'                                                                             => 'Cost',
  'Crea il tuo account'                                                               => 'Create your account',
  'Crea un account e conserva lo storico delle spedizioni'                            => 'Create an account and keep the shipping history',
  'Create Page'                                                                       => 'Create Page',
  'Custom Pages'                                                                      => 'Custom Pages',
  'Custom Pages List'                                                                 => 'Custom Pages List',
  'Customer'                                                                          => 'Customer',
  'Customer Info Update'                                                              => 'Customer Info Update',
  'Customer List'                                                                     => 'Customer List',
  'DESTINATARIO'                                                                      => 'RECIPIENT',
  'Da'                                                                                => 'From',
  'Dashboard'                                                                         => 'Dashboard',
  'Data di ritiro'                                                                    => 'Pick up date',
  'Data di scadenza'                                                                  => 'Expiration date',
  'Data per il ritiro'                                                                => 'Pick-up date',
  'Delete'                                                                            => 'Delete',
  'Denominazione spedizione'                                                          => 'Shipping name',
  'Description'                                                                       => 'Description',
  'Descrivi il contenuto che desideri spedire'                                        => 'Describe the content you wish to send',
  'Desidero ricevere occasionalmente comunicazioni di marketing tramite e-mail'       => 'I occasionally want to receive marketing communications by email',
  'Dichiaro di aver letto e di accettare laa'                                         => 'I declare that I have read and accept laa',
  'Dichiaro di aver letto e di accettare tutti i punti dei'                           => 'I declare that I have read and accept all the points of the',
  'Dimensioni'                                                                        => 'dimensions',
  'Edit Page'                                                                         => 'Edit Page',
  'Edit Pages'                                                                        => 'Edit Pages',
  'Edit Settings'                                                                     => 'Edit Settings',
  'Email'                                                                             => 'E-mail',
  'Esci'                                                                              => 'Go out',
  'Eventuali note'                                                                    => 'Possible notes',
  'Filtra i risultati del preventivo'                                                 => 'Filter the results of the estimate',
  'First Name'                                                                        => 'First Name',
  'Fornisci ulteriori dettagli per avere un prezzo della spedizione accurata'         => 'Provide additional details for an accurate shipping price',
  'From'                                                                              => 'from',
  'Full Name'                                                                         => 'Full Name',
  'Hai già un account? Entra!'                                                        => 'Do you already have an account? Come in!',
  'Hai perso la password'                                                             => 'Have you lost your password?',
  'Hai perso la password?'                                                            => 'Have you lost your password?',
  'Home'                                                                              => 'Home',
  'Home page'                                                                         => 'home page',
  'Il nostro blog'                                                                    => 'Our blog',
  'Il tuo indirizzo email'                                                            => 'Your email',
  'Il tuo nome'                                                                       => 'Your name',
  'In transito'                                                                       => 'In transit',
  'Indirizzi della spedizione'                                                        => 'Shipping addresses',
  'Indirizzo'                                                                         => 'Address',
  'Indirizzo PEC'                                                                     => 'PEC address',
  'Indirizzo email'                                                                   => 'Email address',
  'Indirizzo riga 1'                                                                  => 'Address line 1',
  'Indirizzo riga 2'                                                                  => 'Address line 2',
  'Inserisci gli indirizzi della spezione'                                            => 'Enter the shipping addresses',
  'Inserisci i dati della carta di credito per procedere alla conferma del pagamento' => 'Inserisci i dati della carta di credito per procedere alla conferma del pagamento',
  'Enter your credit card details to confirm the payment.'                            => 'Enter your credit card details to confirm the payment.',
  'Inserisci il cognome'                                                              => 'Enter the surname',
  'Inserisci il nome completo'                                                        => 'Enter your full name',
  'Inserisci il peso'                                                                 => 'Enter your weight',
  'Invia'                                                                             => 'Submit',
  'Invoice Date'                                                                      => 'Invoice Date',
  'Iscriviti alla newsletter e ricevi un buono sconto di 5€ sul primo ordine'         => 'Sign up for the newsletter and receive a 5 € discount voucher on the first order',
  'Key 1'                                                                             => 'Key 1',
  'L"ndirizzo del mittente è diverso dall"indirizzo di partenza della spedizione'     => 'The sender\'s address is different from the starting address of the shipment',
  'La password deve essere composta da almeno 8 caratteri'                            => 'The password must be at least 8 characters long',
  'La tua privacy è al sicuro'                                                        => 'Your privacy is secure',
  'Language Settings'                                                                 => 'Language Settings',
  'Larghezza'                                                                         => 'Width',
  'Last Name'                                                                         => 'Last Name',
  'Location'                                                                          => 'Location',
  'London'                                                                            => 'London',
  'Lunghezza'                                                                         => 'Length',
  'MITTENTE'                                                                          => 'SENDER',
  'MITTENTE: indirizzo di parenza'                                                    => 'SENDER: parity address',
  'Maggiori informazioni'                                                             => 'More information',
  'Meta Description'                                                                  => 'Meta Description',
  'Meta Title'                                                                        => 'Meta Title',
  'Mi serve la fattura'                                                               => 'I need the invoice',
  'Mobile'                                                                            => 'Mobile',
  'New Password'                                                                      => 'New Password',
  'Nome'                                                                              => 'First name',
  'Nome Società'                                                                      => 'Company name',
  'Non hai un account? Crealo qui'                                                    => 'Do not have an account? Create it here',
  'Non hai un account? Registrati!'                                                   => 'Do not have an account? Sign in!',
  'Numero carta di credito'                                                           => 'Credit card number',
  'Numero di cellulare'                                                               => 'Cellphone number',
  'Numero spedizione'                                                                 => 'Shipment number',
  'Old Password'                                                                      => 'Old Password',
  'Order'                                                                             => 'Order',
  'Order Code'                                                                        => 'Order Code',
  'Order List'                                                                        => 'Order List',
  'Order Status'                                                                      => 'Order Status',
  'Orders'                                                                            => 'Orders',
  'Orders Details'                                                                    => 'Orders Details',
  'Ordina i risultati del preventivoo'                                                => 'Order the results of the quote',
  'Paese'                                                                             => 'country',
  'Paga con Carta di Credito'                                                         => 'Pay with credit card',
  'Paga con Paypa'                                                                    => 'Pay with Paypal',
  'Pagamento'                                                                         => 'Payment',
  'Page Content'                                                                      => 'Page Content',
  'Page Slug'                                                                         => 'Page Slug',
  'Page Title'                                                                        => 'Page Title',
  'Pages'                                                                             => 'Pages',
  'Partita iva'                                                                       => 'VAT number',
  'Peso'                                                                              => 'Weight',
  'Preferenze'                                                                        => 'Preferences',
  'Prenota'                                                                           => 'Book',
  'Prenotazione spedizione reicevuta'                                                 => 'Shipment booking received',
  'Price'                                                                             => 'Price',
  'Provincia'                                                                         => 'province',
  'Reclami'                                                                           => 'Complaints',
  'Referente'                                                                         => 'contact',
  'Registrati'                                                                        => 'Sign in',
  'Reimpostazione della password'                                                     => 'Reset your password',
  'Resetta la password'                                                               => 'Reset your password',
  'Ricerca nel sito'                                                                  => 'Search the site',
  'Ricordami la password'                                                             => 'Remember me the password',
  'Ripeti la password'                                                                => 'Repeat password',
  'Risultati della ricerca'                                                           => 'Search results',
  'Ritirato da casa o dal luogo di lavoro'                                            => 'Picked up from home or work',
  'Ritirato presso il cliente'                                                        => 'Collected from the customer',
  'Save'                                                                              => 'Save',
  'Save Settings'                                                                     => 'Save Settings',
  'Scegli una password'                                                               => 'Choose a password',
  'Scrivi eventuali note che vuoi comunicare allo spedizioniere'                      => 'Write any notes you want to communicate to the shipper',
  'Scrivi il nome della società'                                                      => 'Write the name of the company',
  'Scrivi il tuo recapito telefonico'                                                 => 'Write your telephone number',
  'Seleziona il cap'                                                                  => 'Select the cap',
  'Seleziona il paese'                                                                => 'Select the country',
  'Seleziona la città'                                                                => 'Select the city',
  'Seleziona la provincia'                                                            => 'Select the province',
  'Servizi aggiuntivi'                                                                => 'Additional services',
  'Servzio'                                                                           => 'Service',
  'Spedisci'                                                                          => 'Email',
  'Spedisci un pacco'                                                                 => 'Send a parcel',
  'Spedizione per ufficio'                                                            => 'Shipping to the office',
  'Spedizioni aziende'                                                                => 'Shipping companies',
  'Spedizioni internazionali'                                                         => 'International shipments',
  'Status'                                                                            => 'Status',
  'Sub - Total amount'                                                                => 'Sub - Total amount',
  'TOTALE'                                                                            => 'TOTAL',
  'Tentativo di consegna 1'                                                           => 'Delivery attempt 1',
  'Tentativo di consegna 2'                                                           => 'Delivery attempt 2',
  'Termini e Condizioni'                                                              => 'Terms and conditions',
  'Termini e condizioni'                                                              => 'Terms and conditions',
  'Ticket Messages'                                                                   => 'Ticket Messages',
  'Ticket Title'                                                                      => 'Ticket Title',
  'Tickets'                                                                           => 'Tickets',
  'To'                                                                                => 'to',
  'Torna alla Home'                                                                   => 'Back to home',
  'Torna alla homepage'                                                               => 'Back to home Page',
  'Total'                                                                             => 'Total',
  'Total Customers'                                                                   => 'Total Customers',
  'Total Orders'                                                                      => 'Total Orders',
  'Total Tickets'                                                                     => 'Total Tickets',
  'Traccia'                                                                           => 'Track',
  'Traccia un pacco'                                                                  => 'Track a parcel',
  'Undefined'                                                                         => 'Undefined',
  'Update'                                                                            => 'Update',
  'Update Password'                                                                   => 'Update Password',
  'User'                                                                              => 'User',
  'User View'                                                                         => 'User View',
  'Users'                                                                             => 'Users',
  'Valore della merce'                                                                => 'Value of the goods',
  'Volume'                                                                            => 'Volume',
  'We have caught some problem'                                                       => 'We have caught some problem',
  'a'                                                                                 => 'to',
  'right_text'                                                                        => 'All texts and graphics on the site are subject to the copyright laws in force.',
  'home_text_big'                                                                     => 'Compare and book low cost shipping services with TiSpedisco',
  'home_text_small'                                                                   => 'Wherever in the world you want to send your package, we compare the best couriers to offer you the best services and delivery rates.',
  'login'                                                                             => 'Log in',
  'preferito'                                                                         => 'favorite',
  'recensioni'                                                                        => 'reviews',
  'sign_in'                                                                           => 'Sign in',
  'totale della spedizione'                                                           => 'total shipment',
  'vat'                                                                               => 'vat',
  'Update Cost'                                                                       => 'Update Cost',
  "Length"                                                                            => "Length",
  "Height"                                                                            => "Height",
  "Weight"                                                                            => "Weight",

);
